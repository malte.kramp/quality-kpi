"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")

def kpi_mass(system: LegoAssembly)->float:
    """
    Calculates the total mass of the system

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_mass (float): Sum of masses of all components in the system in g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_mass()")

    total_mass = 0
    for c in system.get_component_list(-1):
        total_mass += c.properties["mass [g]"]
    return total_mass # alternative: sum(c.properties["mass [g]"] for c in system.get_component_list(-1))

# Add new functions for calculating metrics

def kpi_price(system: LegoAssembly)->float:
#like in kpi_mass, we just add all the prices within a system to get the total price    
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_price()")
        
    total_price = 0
    for c in system.get_component_list(-1):
        total_price += c.properties["price [Euro]"]
    return total_price

if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
    
def kpi_deliverytime(system: LegoAssembly)->float:
#to get the highest delivery time out of a system,we compare the current highest delivery time to all the other delivery times 
#and replace it once we find one that's bigger
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_deliverytime()")
    
    total_deliverytime = 0
    for c in system.get_component_list(-1):
        if total_deliverytime <= c.properties["delivery time [days]"]:
            total_deliverytime = c.properties["delivery time [days]"] 
    return total_deliverytime
    print(total_deliverytime)
    
if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )